import 'package:flutter/widgets.dart';

const List<Project> projects = [
  Project(
      imgResource: "project00.png",
      description:
          "A fun app made as a personal project where users can login/sign up with accounts and pick parts to build a PC for compatibility checks. Features a very robust authentication system with verification emails and makes use of many Flutter libraries."),
  Project(
      imgResource: "project01.png",
      description:
          "A Flutter client app for a simple API which features some basic use of the BLoC pattern and is capable of GET, POST, UPDATE, PATCH, DELETE operations. (API implemented with Node on a raspberry pi and data hosted on MongoDB)."),
  Project(
      imgResource: "project02.png",
      description:
          "A RESTful API implemented to serve articles or WiKis and is capable of GET, POST, UPDATE, PATCH, DELETE operations. (API implemented with Node on a raspberry pi and data hosted on MongoDB, includes a basic client app made using Flutter)."),
];

class Project {
  final String imgResource;
  final String description;
  const Project({@required this.imgResource, @required this.description});
}
