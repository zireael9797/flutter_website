import 'package:flutter/material.dart';
import 'package:flutter_website/util/html_helper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SocialButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        IconButton(
            icon: Icon(FontAwesomeIcons.gitlab,
                size: 20, color: Colors.deepOrange),
            onPressed: () {
              HtmlHelper.launchUrl("https://gitlab.com/zireael9797");
            }),
        IconButton(
            icon: Icon(FontAwesomeIcons.twitter,
                size: 20, color: Colors.blue[700]),
            onPressed: () {
              HtmlHelper.launchUrl("https://twitter.com/zireael9797");
            }),
        IconButton(
            icon: Icon(FontAwesomeIcons.linkedin,
                size: 20, color: Colors.lightBlue[700]),
            onPressed: () {
              HtmlHelper.launchUrl(
                  "https://www.linkedin.com/in/raiyan-hossain-672185140/");
            }),
        IconButton(
            icon: Icon(FontAwesomeIcons.solidEnvelope,
                size: 20, color: Colors.deepPurple),
            onPressed: () {
              HtmlHelper.launchUrl("mailto:ryanhossain9797@gmail.com");
            }),
      ],
    );
  }
}
