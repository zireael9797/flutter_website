import 'package:flutter/material.dart';
import 'package:flutter_website/util/constants.dart';

typedef OnTabPressed();

class TabButton extends StatelessWidget {
  final int _targetButton;
  final int _currentButton;
  final String _title;
  final OnTabPressed _onPress;
  final bool _buttonEffects;
  const TabButton(
      {Key key,
      @required int targetButton,
      @required int currentButton,
      @required String title,
      @required OnTabPressed onPress,
      bool buttonEffects = false})
      : _targetButton = targetButton,
        _currentButton = currentButton,
        _title = title,
        _onPress = onPress,
        _buttonEffects = buttonEffects,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor:
          _buttonEffects ? Theme.of(context).splashColor : Colors.transparent,
      highlightColor: _buttonEffects
          ? Theme.of(context).highlightColor
          : Colors.transparent,
      hoverColor:
          _buttonEffects ? Theme.of(context).hoverColor : Colors.transparent,
      onTap: () => _onPress(),
      child: Padding(
        padding: kContainerPadding,
        child: Text(
          _title,
          style: TextStyle(
              color:
                  _currentButton == _targetButton ? Colors.white : Colors.grey,
              fontFamily: "Rodin"),
        ),
      ),
    );
  }
}
