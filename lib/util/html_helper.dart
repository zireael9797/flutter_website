import 'dart:html' as html;

class HtmlHelper {
  static launchUrl(String url) {
    html.window.location.href = url;
  }
}
