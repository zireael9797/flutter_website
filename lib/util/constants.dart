import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const kSmallScreenThreshold = 600;

const kSeparatorColor = Colors.black38;

const kContainerPadding = EdgeInsets.all(20);
const kVerticalContainerPadding = EdgeInsets.symmetric(vertical: 20);
const kHorizontalContainerPadding = EdgeInsets.symmetric(horizontal: 20);

const kThreeHundredMillis = Duration(milliseconds: 300);
const kTwoHundredMillis = Duration(milliseconds: 200);

const kDecelerateCurve = Curves.decelerate;
