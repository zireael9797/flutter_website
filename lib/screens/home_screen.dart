import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:flutter_website/components/social_buttons.dart';

import 'package:flutter_website/screens/about_page.dart';
import 'package:flutter_website/screens/projects_page.dart';
import 'package:flutter_website/screens/resume_page.dart';

import 'package:flutter_website/util/constants.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Theme(
      data: Theme.of(context).copyWith(
        textTheme: Theme.of(context).textTheme.apply(fontFamily: "Rodin"),
        primaryTextTheme:
            Theme.of(context).primaryTextTheme.apply(fontFamily: "Rodin"),
        accentTextTheme:
            Theme.of(context).accentTextTheme.apply(fontFamily: "Rodin"),
      ),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(),
                  ),
                  width < kSmallScreenThreshold
                      ? Container(
                          width: 0,
                        )
                      : NavBar(),
                ],
              ),
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: HomeTabs(),
            ),
          ],
        ),
      ),
    );
  }
}

//Contains an Animated Switcher and a List of the Main Pages
class HomeTabs extends StatefulWidget {
  const HomeTabs({
    Key key,
  }) : super(key: key);

  @override
  _HomeTabsState createState() => _HomeTabsState();
}

class _HomeTabsState extends State<HomeTabs> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    List<Widget> pages = [
      //-------------------------------------------ALL PAGES--------------------------------------------
      ResumePage(width: width, key: Key("resumePage")),
      ProjectsPage(width: width, key: Key("projectsPage")),
      AboutPage(width: width, key: Key("aboutPage"))
    ];

    return CarouselSlider(
      items: pages,
      options: CarouselOptions(
          viewportFraction: 1,
          enlargeCenterPage: true,
          autoPlay: false,
          pageSnapping: true,
          scrollPhysics: ClampingScrollPhysics()),
    );
  }
}

//Navigation Bar Buttons for Top Bar Row
class NavBar extends StatelessWidget {
  const NavBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SocialButtons(),
            Container(
              width: 40,
            ),
          ],
        ),
      ),
    );
  }
}
