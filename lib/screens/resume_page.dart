import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_website/util/constants.dart';

class ResumePage extends StatefulWidget {
  const ResumePage({
    Key key,
    @required this.width,
  }) : super(key: key);

  final double width;

  @override
  _ResumePageState createState() => _ResumePageState();
}

class _ResumePageState extends State<ResumePage> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: AnimatedContainer(
        duration: kTwoHundredMillis,
        margin: EdgeInsets.symmetric(
            vertical: 20,
            horizontal:
                widget.width < kSmallScreenThreshold ? 0 : widget.width / 10),
        child: Column(
          children: <Widget>[
            Padding(
              padding: kVerticalContainerPadding,
              child: AnimatedContainer(
                duration: kTwoHundredMillis,
                height: widget.width < kSmallScreenThreshold ? 250 : 200,
                child: Center(
                  child: ClipOval(
                    child: Image.asset(
                      "images/dp.png",
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: kVerticalContainerPadding,
              child: AnimatedSize(
                vsync: this,
                duration: kTwoHundredMillis,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Hi, I'm Raiyan",
                      style: TextStyle(
                        fontFamily: "ShadowsIntoLight",
                        letterSpacing: 1,
                        fontSize:
                            widget.width < kSmallScreenThreshold ? 40 : 60,
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: EdgeInsets.all(40),
                        child: AutoSizeText(
                          "let's make something amazing together",
                          maxLines: 5,
                          minFontSize: 25,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: "Quicksand",
                            letterSpacing: 2,
                            fontSize: 30,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
