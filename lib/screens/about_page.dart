import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({
    Key key,
    @required this.width,
  }) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        key: Key("aboutPage"),
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                Text("Email: ryanhossain9797@gmail.com"),
                Text("Call: 01779579170"),
              ],
            ),
          ),
        ));
  }
}
